class Trinary
  BASE = 3
  VALID_INPUT = /\D|[3-9]/
  
  def initialize(numeral)
    @numeral = numeral
  end

  def to_decimal
    return 0 unless valid_trinary?(@numeral)
    decimal = 0
    @numeral.reverse.chars.each_with_index do |value, index|
      decimal += value.to_i * BASE ** index
    end
    decimal
  end

  def valid_trinary?(num)
    return num =~ VALID_INPUT ? false : true # returns the location of the match or nil
  end
end
