class Octal
  def initialize(octal_string)
    @octal_string = octal_string
  end

  def to_decimal
    return 0 unless valid_octal?(@octal_string)

    decimal = 0
    temp = @octal_string.reverse
    temp.chars.each_with_index do |value, index|
      decimal += value.to_i * 8 ** index
    end
    decimal
  end

  def valid_octal?(num)
    return num =~ /\D|[8-9]/ ? false : true # returns the location of the match or nil
  end
end