test_string = 'whats the matter with kansas.'

def odd_words(str)
  cleaned_str = str.chop.strip
  cleaned_str.split(/\s+/).map.with_index do |word, index|
    if index.odd?
      word.reverse
    else
      word
    end
  end.join(' ').concat('.')
end

puts odd_words(test_string)