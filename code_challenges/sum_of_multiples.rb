require 'pry'

class SumOfMultiples
  def initialize(*multipliers)
    @multipliers = multipliers
  end

  def self.to(n)
   SumOfMultiples.new().to(n)
  end

  def to(n)
    @multipliers = @multipliers.empty? ? [3, 5] : @multipliers
    multiples = []
    (@multipliers.min...n).each do |value|
      @multipliers.each do |m|
        if value % m == 0
          multiples << value if value % m == 0
          break
        end
      end
    end
    multiples.empty? ? 0 : multiples.reduce(:+)
  end 
end
