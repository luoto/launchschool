class Phrase
  SPLIT_POINTS = /\b[\w']+\b/
  def initialize(phrase)
    @phrase = phrase.downcase
  end

  def word_count
    counts = Hash.new(0)
    @phrase.scan(SPLIT_POINTS).each do |word|
      counts[word] += 1
    end
    counts
  end
end