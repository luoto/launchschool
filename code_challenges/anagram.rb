require 'pry'

# Write a program that, given a word and a list of possible anagrams, selects the correct sublist that contains the anagrams of the word.

# For example, given the word "listen" and a list of candidates like "enlists" "google" "inlets" "banana" the program should return a list containing "inlets". Please read the test suite for exact rules of anagrams.

class Anagram
  def initialize(word)
    @word = word
    @letters = letters(@word)
  end

  def match(list)
    list.map { |word| [word, letters(word)] }
        .select{ |array| array.last == @letters && array.first.downcase != @word.downcase }
        .map { |array| array.first }
  end

  def letters(word)
    letter_hash = Hash.new(0)
    word.chars.each { |char| letter_hash[char.downcase] += 1 }
    letter_hash
  end
end
