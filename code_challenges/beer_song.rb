class BeerSong
  def verses(line_start, line_end)
    segment = []
    line_start.downto(line_end) do |line|
      segment.push(verse(line))
    end
    segment.join("\n")
  end

  def verse(n)
    case n
    when (3..99) then "#{n} bottles of beer on the wall, #{n} bottles of beer.\nTake one down and pass it around, #{n - 1} bottles of beer on the wall.\n"
    when 2 then "#{n} bottles of beer on the wall, #{n} bottles of beer.\nTake one down and pass it around, #{n - 1} bottle of beer on the wall.\n"
    when 1 then "#{n} bottle of beer on the wall, #{n} bottle of beer.\nTake it down and pass it around, no more bottles of beer on the wall.\n"
    when 0 then "No more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, 99 bottles of beer on the wall.\n"
    end
  end

  def lyrics
    song = []
    99.downto(0) do |verse_number|
      song.push(verse(verse_number))
    end
    song.join("\n")
  end
end
