# Atbash ciper is an ancient substitution cipher originating from the Middle East. It substitutes the first letter with the last, second letter with the second last, etc.
class Atbash
  FIRST13 = { 'a' => 'z', 'b' => 'y', 'c' => 'x', 'd' => 'w', 'e' => 'v',
              'f' => 'u', 'g' => 't', 'h' => 's', 'i' => 'r', 'j' => 'q',
              'k' => 'p', 'l' => 'o', 'm' => 'n' }.freeze
  LAST13 = FIRST13.map { |key, value| [value, key] }.to_h.freeze
  ENCRYPT = FIRST13.merge(LAST13).freeze
  SPACE_INTERVAL = 5
  SEPERATOR = ' '.freeze

  def self.encode(plaintext)
    cleantext = plaintext.downcase.gsub(/[^a-z0-9]/, '')
    encrypted_text = cleantext.chars.map do |char|
      char =~ /[a-z]/ ? ENCRYPT[char] : char
    end.join
    format(encrypted_text)
  end

  def self.format(text)
    spaces_required = text.length / SPACE_INTERVAL
    1.upto(spaces_required) do |i|
      idx = i * SPACE_INTERVAL + i - 1 # adjusts for growing string
      text.insert(idx, SEPERATOR)
    end
    text.strip # may include an extra space if text is a multiple of 5
  end
end
