require 'pry'

class PerfectNumber
  def self.classify(n)
    raise RuntimeError if n < 0
    sum = PerfectNumber.sum(PerfectNumber.factors(n))
    case
    when (sum < n) then return 'deficient'
    when (sum == n) then return 'perfect'
    when (sum > n) then return 'abundant'
    end
  end

  def self.factors(n)
    (1..(n / 2)).select { |i| n % i == 0 }
  end

  def self.sum(list)
    list.reduce(:+)
  end
end