require 'pry'

class DNA
  def initialize(strand)
    @strand = strand
  end

  def hamming_distance(other_strand)
    max_distance = [@strand.length, other_strand.length].min
    distance, idx = 0, 0
    loop do
      break if idx == max_distance
      distance += 1 if mismatch?(@strand[idx], other_strand[idx])
      idx += 1
    end
    distance
  end

  def mismatch?(a, b)
    a != b
  end
end
