require 'pry'

class Sieve
  def initialize(n)
    @last = n
    @nums = {}
    (2..n).to_a.each { |i| @nums[i] = i }
  end

  def primes
    p = 2
    loop do
      multiplier = 2
      loop do
        @nums[p * multiplier] = nil
        multiplier += 1
        break if p * multiplier > @last
      end
      p += 1
      break if p > @last
    end
    @nums.map{ |k, v| v }.select { |v| !v.nil? }
  end
end