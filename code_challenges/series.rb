class Series
  def initialize(digits)
    @digits = digits
  end

  def slices(n)
    raise ArgumentError if n > @digits.length
    result = []
    current_index = 0
    loop do
      inner_array = []
      result << @digits[current_index...(current_index + n)].chars.map(&:to_i)
      current_index += 1
      break if current_index + n > @digits.length
    end
    result
  end
end
